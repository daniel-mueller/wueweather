wueweather
==========

Simple C program that fetches weatherdata from Yahoo with libcurl and libxml2

Compile with: 
```
 gcc  weather2.c -I /usr/include/libxml2 -lcurl -lxml2
```

This was done for my personal amusement and shouldn't be used as an example for proper code.
