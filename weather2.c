#include <curl/curl.h> 
#include <stdlib.h>
#include <string.h>
#include <libxml/xmlmemory.h>
#include <libxml/parser.h>


#define BUFFER_SIZE (256 * 1024) /* 256kB */

xmlDocPtr doc;
xmlNodePtr cur;

struct write_result {
	char *data;
	int pos;
};

void parseSecond (xmlDocPtr doc, xmlNodePtr cur) {

	xmlChar *key;	
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"description"))) {
			key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		    printf("%s\n", key);
		    xmlFree(key);    
 	    }
	cur = cur->next;
	}
    return;
}

void parseFirst (xmlDocPtr doc, xmlNodePtr cur) {

	xmlChar *key;
	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"title"))) {
		    key = xmlNodeListGetString(doc, cur->xmlChildrenNode, 1);
		    printf("%s\n", key);
		    xmlFree(key);
		    
 	    }
 	    if ((!xmlStrcmp(cur->name, (const xmlChar *)"item"))) {
		    parseSecond(doc, cur);	    
 	    }
	cur = cur->next;
	}
    return;

}

static size_t curl_write( void *ptr, size_t size, size_t nmemb, void *stream) {

	struct write_result *result = (struct write_result *)stream;

	if(result->pos + size * nmemb >= BUFFER_SIZE - 1) {
		fprintf(stderr, "curl error: too small buffer\n");
		return 0;
	}

	memcpy(result->data + result->pos, ptr, size * nmemb);
	result->pos += size * nmemb;

	return size * nmemb;
} 

int main(int argc, char **argv) {
	CURL *curl = curl_easy_init(); 
	char *data;

	data = malloc(BUFFER_SIZE);
	if (! data)
	fprintf(stderr, "Error allocating %d bytes.\n", BUFFER_SIZE);

	struct write_result write_result = {
		.data = data,
		.pos = 0
	};

	curl_easy_setopt(curl, CURLOPT_URL, "http://weather.yahooapis.com/forecastrss?w=708287&u=c");
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, curl_write);
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &write_result);

	curl_easy_perform(curl);

	data[write_result.pos] = '\0';

	doc = xmlParseDoc(data);
	if (doc == NULL ) {
		fprintf(stderr,"Document not parsed successfully. \n");
		return;
	}

	cur = xmlDocGetRootElement(doc);
	if (cur == NULL) {
		fprintf(stderr,"empty document\n");
		xmlFreeDoc(doc);
		return;
	}

	cur = cur->xmlChildrenNode;
	while (cur != NULL) {
		if ((!xmlStrcmp(cur->name, (const xmlChar *)"channel"))){
			parseFirst(doc, cur);
		}
		 
	cur = cur->next;
	}

	free(data);
	curl_easy_cleanup(curl);
	curl_global_cleanup();
}
